===========================
C2Games.Opfor Release Notes
===========================

.. contents:: Topics


v0.1.1
======

Release Summary
---------------

Minor Bug Fixes

Bugfixes
--------

- Convert use of deprecated `Display.warn` to `Display.warning` (https://gitlab.com/c2-games/red-team/c2games.opfor/-/issues/9)
- Remove mongo/db dependency from lookup module (https://gitlab.com/c2-games/red-team/c2games.opfor/-/issues/8)


v0.1.0
======

Release Summary
---------------

Initial Release; No detailed Changelog available
