#!/usr/bin/python3
from ansible.plugins.action import ActionBase
from ansible.utils.display import Display
from ansible_collections.c2games.opfor.plugins.module_utils.c2games import C2GamesPlugin

display = Display()


class ActionModule(C2GamesPlugin, ActionBase):
    def update_db(self, update, task_vars, **kwargs):
        """
        Update the database with an update, if available
        :param update: Object to insert to DB
        :param task_vars: Ansible TaskVar object
        :param kwargs: Arguments to _update_db
        :return dict: Document, with an added _id attribute if it didn't already exist in the dict
        """
        return self._update_db(plugin='register_plugin', update=update, task_vars=task_vars, **kwargs)

    def run(self, tmp=None, task_vars=None):
        """
        :param str tmp: Deprecated parameter.  This is no longer used.  An action plugin that calls
            another one and wants to use the same remote tmp for both should set
            self._connection._shell.tmpdir rather than this parameter.
        :param dict task_vars: The variables (host vars, group vars, config vars,
            etc) associated with this task.
        :returns dict: Results. https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html

        Implementors of action modules may find the following variables especially useful:
          * Module parameters. These are stored in self._task.args
        """
        result = super(ActionModule, self).run(tmp, task_vars)
        del tmp  # deprecated

        # Load config if we haven't already
        self.runtime_begin(task_vars)

        # Handle module args
        module_args = self._task.args.copy()

        self.update_db({**module_args}, task_vars)

        if module_args.get('debug') is not None:
            result['msg'] = str(module_args)
            # force flag to make debug output always verbose
            result['_ansible_verbose_always'] = True

        return result
