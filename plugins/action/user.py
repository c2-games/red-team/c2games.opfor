from __future__ import (absolute_import, division, print_function)  # is this for python2 compatibility?

import crypt
from ansible.errors import AnsibleRuntimeError
from ansible.plugins.action import ActionBase
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from ansible_collections.c2games.opfor.plugins.module_utils.c2games import C2GamesPlugin
from ansible_collections.c2games.opfor.plugins.module_utils.users import UserList

display = Display()

# DOCUMENTATION = """
# lookup: user
# author: Brodie Davis <davisb@c2games.org>
# version_added: "0.2.0"
# short_description: Retrieve a user and document it's use
# description:
#     - Return a user based on the criteria specified, and logs to the C2Games database if it's available.
#       The default c2games configuration file is at /etc/ansible/c2games.json, and can be specified/overridden
#       using the environment variable or ansible variable C2GAMES_CONFIG.
# # options: TODO options
# notes:
#   - does not work correctly with `lookup()` - lookup will return a list of dict keys
#   - only a single tag may be specified TODO this will be updated later
# env:
#   - c2games_event_id
#   - c2games_configuration_id
#   - C2GAMES_CONFIG
# """


class ActionModule(C2GamesPlugin, ActionBase):
    def run(self, tmp=None, task_vars=None):
        """
        Search the DB for the given user specifications, returning one or more users
        """
        result = super(ActionModule, self).run(tmp, task_vars)
        del tmp  # deprecated

        # Load config if we haven't already
        self.runtime_begin(task_vars)

        # Handle module args
        module_args = self._task.args.copy()
        event_id, config_id = self._get_ids(task_vars)

        if not module_args:
            raise AnsibleRuntimeError("No user specifications passed for lookup")

        if not module_args.get('user_list'):
            raise AnsibleRuntimeError("user_list name must be supplied")

        # TODO: support checking event_id for previously used users
        # task = self._update_db(plugin="action.user", update={'opts': module_args}, task_vars=task_vars, status='executing')
        user_list = UserList(self.db)
        users = user_list.search_users(**module_args)

        if not users:
            reason = 'failed to find users matching query'
            # self._update_db(plugin="user", update=task, task_vars=task_vars, status='failed', reason=reason)
            raise AnsibleRuntimeError(reason)

        # If we found a valid list of users, and caller doesnt want list, send back 1
        ret = []
        for user in users:
            for group in user.get('groups', []):
                g_res = self._execute_module(module_name='ansible.builtin.group', module_args={'name': group}, task_vars=task_vars)
                if g_res.get('failed'):
                    display.warning('failed to add group: {}'.format(g_res))
                else:
                    display.v('added group: {}'.format(g_res))
            res = self._execute_module(module_name='ansible.builtin.user', module_args={
                'append': True,
                'non_unique': True,
                'uid': user.get("uid"),
                'name': user.get('username'),
                'comment': user.get('full_name'),
                'groups': user.get('groups'),
                'password': crypt.crypt(user.get('password', 'password')),
                'update_password': 'on_create'
            }, task_vars=task_vars)
            display.v('got response from adding user: {}'.format(res))
            user.update(res)
            self._update_db(plugin="action.user", update=user, task_vars=task_vars, status='executed')
        return result
