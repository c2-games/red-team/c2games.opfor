#!/usr/bin/python3
from ansible.errors import AnsibleError
from ansible.plugins.action import ActionBase
from ansible.utils.display import Display
from ansible_collections.c2games.opfor.plugins.module_utils.c2games import C2GamesPlugin

display = Display()

# Documentation is in plugins/modules/persistence.py


class ActionModule(C2GamesPlugin, ActionBase):
    def update_db(self, update, task_vars, **kwargs):
        """
        Update the database with an update, if available
        :param update: Object to insert to DB
        :param task_vars: Ansible TaskVar object
        :param kwargs: Arguments to _update_db
        :return dict: Document, with an added _id attribute if it didn't already exist in the dict
        """
        return self._update_db(plugin='persistence', update=update, task_vars=task_vars, **kwargs)

    @staticmethod
    def _get_tasks(task_vars, event_id, config_id):
        """
        Host Variables for the Current Host.
        :param TaskVars task_vars: Dictionary of ansible facts for the current host
        :param str event_id: C2Games Event ID
        :param str config_id: C2Games Config ID
        :return List[dict]: List of persistence tasks
        """
        facts = task_vars['ansible_facts']
        display.vvv(str(facts.get('c2games', {})))
        ret = facts.get('c2games', {}).get(event_id, {}).get(config_id, {}).get('persistence', [])
        display.vvv(str(ret))
        return ret

    def register(self, task_vars, args, config_id, event_id):
        # Validate required args
        if 'path' not in args:
            return {"msg": "path parameter is required", "failed": True}

        # Update the DB if we're connected
        args = self.update_db(args, task_vars, opts=args, action='register', status='pending')

        return {
            'msg': 'Persistence registered successfully',
            # return updated Ansible Facts. This will be stored on the host
            'ansible_facts': {
                'c2games': {
                    event_id: {
                        config_id: {
                            'persistence': self._get_tasks(task_vars, event_id, config_id) + [args]
                        }
                    }
                }
            }
        }

    def _cron(self, task_vars, tasks, args):
        """
        Add a cron job using the module parameters for each registered persistence file path.
        Module arguments are passed directly to the ansible.builtin.cron module.

        :param dict args: Dictionary of args to pass to cron module
        :param dict task_vars: Dictionary of Ansible task_vars
        :return: return of module
        """
        # todo figure out how to rm the #ansible: (Name) before each cron entry
        # todo utilize this feature to not duplicate cron jobs? then cleanup during final 'cover_our_tracks'
        ret = []
        for task in tasks:
            cmd_path = task.get('path')
            cmd_args = task.get('args', "")

            if not cmd_path:
                raise AnsibleError("path is required for registered persistence tasks")

            args.setdefault('job', "%s %s" % (cmd_path, cmd_args))
            # save initial task to DB
            obj = self.update_db({'task': task}, task_vars, opts=args, action='persistence.cron', status='executing')
            # todo catch when cron goes wrong, update task in DB and return an erroneous value
            ret.append(self._execute_module(module_name='ansible.builtin.cron', module_args=args, task_vars=task_vars))
            # Update task status
            self.update_db(obj, task_vars, status='executed')
            # todo clean up the ansible ID left in the cron file. Here? End of Play/Job?
        return ret

    def run(self, tmp=None, task_vars=None):
        """
        :param str tmp: Deprecated parameter.  This is no longer used.  An action plugin that calls
            another one and wants to use the same remote tmp for both should set
            self._connection._shell.tmpdir rather than this parameter.
        :param dict task_vars: The variables (host vars, group vars, config vars,
            etc) associated with this task.
        :returns dict: Results. https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html

        Implementors of action modules may find the following variables especially useful:
          * Module parameters.  These are stored in self._task.args
        """
        result = super(ActionModule, self).run(tmp, task_vars)
        del tmp  # deprecated

        # Load config if we haven't already
        self.runtime_begin(task_vars)

        # Handle module args
        module_args = self._task.args.copy()
        event_id, config_id = self._get_ids(task_vars)

        # Register a new path
        if module_args.get('register'):
            # to register new Ansible Facts, they must be returned from this function as {'ansible_facts': {}}
            result.update(self.register(task_vars, module_args['register'], event_id, config_id))
            # Return before checking for valid registrations
            return result

        tasks = self._get_tasks(task_vars, config_id, event_id)
        if not tasks:
            result.update({
                'failed': True, 'changed': False,
                'msg': 'No persistence tasks registered! '
                       'Use c2games.opfor.persistence with the "register" keyword to register tasks.'
            })
            return result

        # Add persistence using cron
        if module_args.get('cron'):
            result['cron'] = self._cron(task_vars, tasks, module_args['cron'])

        if module_args.get('debug') is not None:
            result['msg'] = str(tasks)
            # force flag to make debug output always verbose
            result['_ansible_verbose_always'] = True

        # todo Add persistence using at
        # if module_args.get('at'):
        #     result['at'] = self._at(module_args['cron'], task_vars)

        return result
