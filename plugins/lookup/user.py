from __future__ import (absolute_import, division, print_function)  # is this for python2 compatibility?

from ansible.errors import AnsibleRuntimeError
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from ansible_collections.c2games.opfor.plugins.module_utils.c2games import C2GamesPlugin
from ansible_collections.c2games.opfor.plugins.module_utils.users import UserList

display = Display()

DOCUMENTATION = """
lookup: user
author: Brodie Davis <davisb@c2games.org>
version_added: "0.2.0"
short_description: Retrieve a user and document it's use
description:
    - Return a user based on the criteria specified, and logs to the C2Games database if it's available.
      The default c2games configuration file is at /etc/ansible/c2games.json, and can be specified/overridden
      using the environment variable or ansible variable C2GAMES_CONFIG.
# options: TODO options
notes:
  - does not work correctly with `lookup()` - lookup will return a list of dict keys
  - only a single tag may be specified TODO this will be updated later
env:
  - c2games_event_id
  - c2games_configuration_id
  - C2GAMES_CONFIG
"""


class LookupModule(C2GamesPlugin, LookupBase):
    def run(self, terms=None, variables=None, wantlist=False, **kwargs):
        """
        Search the DB for the given user specifications, returning one or more users
        """
        if kwargs:
            # kwargs means we were called with query() directly
            opts = kwargs
        else:
            # no kwargs means we were called using the 'with_' keyword
            opts = terms

        if not opts:
            raise AnsibleRuntimeError("No user specifications passed for lookup")

        if not opts.get('user_list'):
            raise AnsibleRuntimeError("user_list name must be supplied")

        # TODO: support checking event_id for previously used users
        task = self._update_db(plugin="lookup.user", update={'opts': opts}, task_vars=variables, status='executing')
        user_list = UserList(self.db)
        users = user_list.search_users(**opts)

        if not users:
            reason = 'failed to find users matching query'
            self._update_db(plugin="user", update=task, task_vars=variables, status='failed', reason=reason)
            raise AnsibleRuntimeError(reason)

        # If we found a valid list of users, and caller doesnt want list, send back 1
        if not wantlist and isinstance(users, list) and users:
            users = users[0]
            task.update(users)
        else:
            task['users'] = users

        self._update_db(plugin="user", update=task, task_vars=variables, status='executed')
        return users
