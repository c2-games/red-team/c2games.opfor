from __future__ import (absolute_import, division, print_function)  # is this for python2 compatibility?

from pathlib import Path
from random import choice, randint
from string import ascii_letters

from ansible.errors import AnsibleRuntimeError
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display
from ansible_collections.c2games.opfor.plugins.module_utils.c2games import C2GamesPlugin

display = Display()


DOCUMENTATION = """
lookup: file
author: Brodie Davis <davisb@c2games.org>
version_added: "0.2.0"
short_description: Record a file's use, and optionally modify the destination path attributes
description:
    - Saves the usage of a file to the C2games database, if available, and modifies the
      returned file's attributes based on the options passed
options:
  file:
    description: Get file path (can be obfuscated or random dir on PATH)
    suboptions:
      src:
        required: false
        type: path
        description: Source file to be copied
      dest:
        required: false
        type: path
        description: Binary name, or full path and binary name to copy to
      on_path:
        required: false
        type: bool
        description: Put the file in a random directory on PATH
      obfuscate:
        required: false
        type: bool
        description: Create a random name for the file when copied
      hidden:
        required: false
        type: bool
        description: Hide the file by adding a . to the file name
      mode:
        required: false
        type: string
        description: File mode the file should end up becoming
      executable:
        required: false
        type: bool
        description: Set file mode to 777

notes:
  - the parent option overrides the on_path option

requirements:
  - pymongo

env:
  - c2games_event_id
  - c2games_configuration_id
  - C2GAMES_CONFIG
"""


class LookupModule(C2GamesPlugin, LookupBase):
    def run(self, terms=None, variables=None, wantlist=False, **kwargs):
        """
        Return a relative or absolute file path from given parameters.

        src (str) - Source file to be copied
        dest (str) - Binary name, or full path and binary name to copy to
        on_path (bool) - Return a full filepath within PATH on the host.
        obfuscate (bool) - Set the filename to a random sequence of numbers/letters.
        hidden (bool) - Start the filename with a '.'
        mode (str) - File mode the file should end up becoming
        executable (bool) - Set file mode to 777
        """
        self.runtime_begin(variables)

        # todo how can we support loops with this?
        if kwargs:
            # kwargs means we were called with query() directly
            opts = kwargs
        else:
            # no kwargs means we were called using the 'with_' keyword
            opts = terms

        if not opts:
            raise AnsibleRuntimeError("No file specifications passed for lookup")

        display.vvv(opts)
        src = Path(opts.get('src'))
        dest = Path(opts.get('dest', opts.get('src')))
        name = dest.name
        parent = dest.parent
        mode = opts.get('mode', '666')

        if isinstance(mode, int):
            display.warning('integers should not be used with mode parameter! You should quote it.')
        if opts.get('obfuscate'):
            name = self.get_random_name()
        if opts.get('on_path'):
            parent = self.get_from_path(variables)
        if opts.get('hidden'):
            name = Path('.{}'.format(str(name)))
        if opts.get('executable'):
            if opts.get('mode'):
                display.warning("executable should not be used with mode parameter! mode takes precedent")
            else:
                mode = '777'

        ret = {
            **opts,  # opts sets the default values, then the options below overrides it
            'src': str(src),
            'dest': str(Path(parent, name)),
            'mode': mode,
        }

        self._update_db(plugin="file", update=ret, task_vars=variables)

        if wantlist:
            ret = [ret]

        return ret

    @staticmethod
    def get_random_name():
        """
        Create a random name from 3 to 15 characters long
        """
        ret = ""
        for _ in range(0, randint(3, 15)):
            ret = ret + choice(ascii_letters)

        return ret

    @staticmethod
    def get_from_path(variables):
        """
        return a random path from PATH
        """
        paths = variables.get('ansible_facts', {}).get('env', {}).get('PATH', '/bin:').split(':')
        return paths[randint(0, len(paths) - 1)]
