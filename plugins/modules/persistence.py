DOCUMENTATION = """
---
module: persistence
author: Brodie Davis <davisb@c2games.org>
version_added: "0.2.0"  # for collections, use the collection version, not the Ansible version
short_description: Run a file at regular intervals
description:
    - Generic way to run a file at regular intervals defined by task options, using builtin system functionality.
      Files must first be registered using the register option. This module should be called again once all
      files intended for persistence are registered. Persistence files are stored first by role_name,
      then by "c2games_configuration_id", if it is available.
options:
  register:
    description: Register a filepath for setting up with Persistence
    suboptions:
      path:
        description: Filepath to set up for persistence
        type: path
        required: true
      args:
        description: Arguments the file should be executed with
        type: string
  cron:
    description: Set up persistence using cron. Additional options are passed to ansible.builtin.cron.
                 TODO inherit options from ansible.builtin.cron
    required: False
notes:
  - Passes all options from persistence keyword (cron/at/etc) to builtin ansible function
  - File paths (and arguments) must be registered first by using the register option
    before calling a persistence method, or an error will be raised
  - Register and persistence methods cannot be used at the same time
env:
  - c2games_event_id
  - c2games_configuration_id
  - C2GAMES_CONFIG
ini:
  section: c2games
"""


