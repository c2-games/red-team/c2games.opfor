from random import randint

from ansible.utils.display import Display

display = Display()


class User:
    def __init__(self, username=None, full_name=None, password=None, tag=None,
                 tags=None, groups=None, auth=None, user_list=None, uid=None,
                 gid=None):
        """
        Dataclass for User. Implements attributes below when initialized with the
        corresponding kwargs.
        """
        self.username = username
        self.full_name = full_name
        self.password = password
        self.tag = tag
        self.tags = tags
        self.groups = groups
        self.auth = auth
        self.user_list = user_list
        self.uid = uid
        self.gid = gid

    @property
    def name(self) -> str:
        """Shorthand for username"""
        return self.username


class UserList:
    """
    Initialize new user list from a file path
    """
    def __init__(self, db=None, users=None, previous_users=None):
        self.db = db
        self.users = users or []
        self.previous_users = previous_users or []

    def __bool__(self) -> bool:
        """
        Return if the user list is empty or not
        """
        return len(self.users) > 0

    def __iter__(self):
        """
        Allows for: `for user in UserList:`
        """
        return iter(self.users)

    def add_user(self, user: dict):
        """
        Add a user to the <UserList> and perform necessary validation
        """
        content = {}
        content['username'] = user['username']
        content['full_name'] = user.get('full_name', user['username'])
        content['password'] = user.get('password', "")
        content['tag'] = user.get('tag', "")
        content['tags'] = user.get('tags', [])
        content['groups'] = user.get('groups', [])
        content['auth'] = user.get('auth', "")
        content['uid'] = user.get('uid', None)
        content['gid'] = user.get('gid', None)
        content['user_list'] = self.name

        self.users.append(User(**content))

    def search_users(self, **kwargs) -> list:
        """
        Search for users in the DB using MongoDB specific search parameters
        """
        if not self.db:
            display.warning("DB not connected!")
            return [{"username": "user", "password": "password", "group": "generic"}]

        to_search = dict()
        limit = kwargs.pop('limit', 1)
        # Remove any None/Falsey kwargs
        for prop in kwargs:
            # Use MongoDBs special notation for searching from words in arrays
            if isinstance(kwargs[prop], list):
                to_search[prop] = {'$in': kwargs[prop]}
            elif kwargs[prop]:
                to_search[prop] = kwargs[prop]

        display.vv('query: {}'.format(to_search))
        display.vvv('limit: {}'.format(limit))

        display.vvv('results: {}'.format(self.db.users.find().count()))

        cursor = self.db.users.find(to_search)
        display.vv('# results: {}'.format(cursor.count()))
        cursor.limit(limit)

        users = list(cursor)
        for user in users:
            del user['_id']
        display.vvv('results: {}'.format(users))

        return users

    def get_random_user(self, no_previous_users=False, username=None, tags=None, groups=None, auth=None):
        """
        Get a random user from the DB

        Raises:
            ValueError - When it is specified to not get previous users, but all users are previously used
        """
        results = list(self.search_users(username=username, tags=tags, groups=groups, auth=auth))
        index = randint(0, len(results) - 1)
        user = results[index]
        if no_previous_users:
            while user in self.previous_users:
                results.pop(index)
                index = randint(0, len(results) - 1)
                user = results[index]

        if user not in self.previous_users:
            self.previous_users.append(user)

        return user
