import json
import os
from pathlib import Path

from ansible.utils.display import Display
display = Display()


class C2GamesPlugin:
    def __init__(self, global_config='/etc/ansible/c2games.cfg.json', *args, **kwargs):
        """
        Inhertiable class to provide access to C2games config file and databse
        *
        * WARNING! You must inherit from this first, THEN Ansible, because Ansible does not call super(). Ex:
        * Wrong: class ActionModule(ActionBase, C2GamesPlugin):
        * Right: class ActionModule(C2GamesPlugin, ActionBase):
        *
        """
        self._mongo = None
        self.db = None
        self.config = {}
        self.config_file = None
        self._runtime_connection_attempted = False

        self._load_config(os.getenv('C2GAMES_CONFIG') or global_config)
        super().__init__(*args, **kwargs)

    @staticmethod
    def _get_ids(task_vars):
        """
        Get Event ID and Config ID from Task Vars
        :param task_vars: Task Variable Dictionary
        :return Tuple(str, str): Tuple with event_id, config_id
        """
        config_id = task_vars.get('c2games', {}).get('config_id', 'DEFAULT')
        event_id = task_vars.get('c2games', {}).get('event_id', 'DEFAULT')

        if 'DEFAULT' == event_id:
            display.warning('using default event_id!')
        if 'DEFAULT' == config_id:
            display.warning('using default config_id!')

        return event_id, config_id

    def _load_config(self, path):
        """
        Attempt to load the config file
        :param str path: Path to the config
        :return bool: Whether the config could be loaded
        """
        if not Path(path).exists():
            display.warning('config path not found: %s' % path)
            return False

        display.vv('loading config %s' % path)
        try:
            with open(path) as f:
                self.config = json.loads(f.read())
            self.config_file = path
        except Exception as e:
            display.warning('failed to load config: %s' % str(e))
            return False

        display.vvvv(str(self.config))
        self._connect_db()
        return True

    def _connect_db(self):
        try:
            display.vv('importing pymongo')
            from pymongo import MongoClient
            display.vv('connecting to MongoDB')
            self._mongo = MongoClient(
                self.config.get('database', {}).get('host', "127.0.0.1"),
                self.config.get('database', {}).get('port', 27017),
                username=self.config.get('database', {}).get('username'),
                password=self.config.get('database', {}).get('password'),
                serverSelectionTimeoutMS=3000,  # 3 seconds
            )
            self._mongo.server_info()
            self.db = self._mongo[self.config.get('database', {}).get('database', 'c2games')]
        except Exception as e:
            display.warning('failed to connect to database: %s' % str(e))
            self._mongo = None

    def runtime_begin(self, task_vars):
        """
        Start runtime exection. Currently, just attempt to connect to
        the DB once during runtime, if we're not already connected
        """
        # Load config if we haven't already
        if not self._runtime_connection_attempted and not self.config and task_vars.get('C2GAMES_CONFIG'):
            display.v('Attempting runtime load of config')
            self._runtime_connection_attempted = True  # don't try to connect to DB more than once
            self._load_config(task_vars['C2GAMES_CONFIG'])

    def _update_db(self, plugin, update, task_vars, action=None, status=None, reason=None, opts=None):
        """
        Insert a dict into the DB, if available
        :param str plugin: String representing the C2Games.opfor Ansible plugin making the update
        :param dict update: Dictionary of attributes to update
        :param TaskVars task_vars: Ansible task vars
        :param str action: Action taken
        :param str status: Status of the action
        :param str reason: Reason for status, if relevant
        :param dict opts: Dictionary of options passed into plugin
        :return dict: Update object, with an added _id attribute if it didn't already exist within update
        """
        if not self.db:
            display.vv('not connected to DB')
            return update

        try:
            database = self.config.get('database', {}).get('database', 'c2games')
            collection = self.config.get('database', {}).get('runtime_collection', 'runtime')
            event_id, config_id = self._get_ids(task_vars)
            obj = {
                '_role': task_vars.get('role_name'),  # allow role to be overridden for now
                **update,
                '_event_id': event_id,
                '_config_id': config_id,
                '_inventory_hostname': task_vars.get('inventory_hostname'),
                '_role_uuid': task_vars.get('role_uuid'),
                '_plugin': plugin
            }
            if action:
                obj['_action'] = action
            if status:
                obj['_status'] = status
            if reason:
                obj['_reason'] = reason
            if opts:
                obj['opts'] = opts

            if update.get('_id'):
                display.vv("updating existing object with _id={}".format(str(update['_id'])))
                from bson import ObjectId
                del obj['_id']
                self.db[database][collection].update_one({'_id': ObjectId(update['_id'])}, {'$set': obj})
            else:
                result = self.db[database][collection].insert_one(obj)
                update['_id'] = str(result.inserted_id)
                display.vv("inserted new object with _id={}".format(str(update['_id'])))
            display.v('database successfully updated!')
        except Exception as e:
            display.warning('failed to update db: %s' % str(e))

        return update
