# Ansible Collection - c2games.opfor

C2Games Opfor Ansible Collection

# Development

Create a link from an Ansible collections path to your development repo:

```bash
mkdir -p ~/.ansible/collections/ansible_collections/c2games/
ln -s ~/Dev/c2games/c2games.opfor/ ~/.ansible/collections/ansible_collections/c2games/opfor
```

# Release

1. Increment Version in `galaxy.yml`
2. Create a changelog fragment `{vers}-release.yml` with release_summary key
3. Run `antsibull-changelog lint`
4. Run `antsibull-changelog release`
5. Commit `CHANGELOG.rst`, `galaxy.yml`, and now-empty `fragments/` folder
6. Build Release: `ansible-galaxy collection build -f`
7. Publish Release: `ansible-galaxy collection publish --token TOKEN c2games-opfor-VERS.tar.gz`

